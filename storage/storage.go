package storage

import (
	"context"
	"ibron/ibron_go_user_service.git/genproto/user_service"
	"ibron/ibron_go_user_service.git/models"
)

type StorageI interface {
	CloseDB()
	User() UserRepoI
}

type UserRepoI interface {
	Create(ctx context.Context, req *user_service.CreateUser) (resp *user_service.UserPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *user_service.UserPrimaryKey) (resp *user_service.User, err error)
	GetAll(ctx context.Context, req *user_service.GetListUserRequest) (resp *user_service.GetListUserResponse, err error)
	Update(ctx context.Context, req *user_service.UpdateUser) (rowsAffected int64, err error)
	UpdatePatch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *user_service.UserPrimaryKey) error
	Login(ctx context.Context, req *user_service.LoginReq) (*user_service.User, error)
	ChechExist(ctx context.Context, req *user_service.CheckExist) (*user_service.CheckExistRes, error)
}
