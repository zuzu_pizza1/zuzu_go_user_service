package grpc

import (
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"ibron/ibron_go_user_service.git/config"
	"ibron/ibron_go_user_service.git/genproto/user_service"
	"ibron/ibron_go_user_service.git/grpc/client"
	"ibron/ibron_go_user_service.git/grpc/service"
	"ibron/ibron_go_user_service.git/pkg/logger"
	"ibron/ibron_go_user_service.git/storage"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	user_service.RegisterUserServiceServer(grpcServer, service.NewUserService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
