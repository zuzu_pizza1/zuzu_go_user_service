package service

import (
	"context"
	"errors"
	"ibron/ibron_go_user_service.git/config"
	"ibron/ibron_go_user_service.git/genproto/user_service"
	"ibron/ibron_go_user_service.git/grpc/client"
	"ibron/ibron_go_user_service.git/models"
	"ibron/ibron_go_user_service.git/pkg/logger"
	"ibron/ibron_go_user_service.git/storage"

	"github.com/golang/protobuf/ptypes/empty"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type UserService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedUserServiceServer
}

func NewUserService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvs client.ServiceManagerI) *UserService {
	return &UserService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvs,
	}
}

func (i *UserService) Create(ctx context.Context, req *user_service.CreateUser) (resp *user_service.User, err error) {

	i.log.Info("---CreateUser------>", logger.Any("req", req))

	pKey, err := i.strg.User().Create(ctx, req)
	if err != nil {
		i.log.Error("!!!CreateUser->User->Create--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	resp, err = i.strg.User().GetByPKey(ctx, pKey)
	if err != nil {
		i.log.Error("!!!GetByPKeyUser->User->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *UserService) GetById(ctx context.Context, req *user_service.UserPrimaryKey) (resp *user_service.User, err error) {

	i.log.Info("---GetUserByID------>", logger.Any("req", req))

	resp, err = i.strg.User().GetByPKey(ctx, req)
	if err != nil {
		i.log.Error("!!!GetUserByID->User->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *UserService) GetList(ctx context.Context, req *user_service.GetListUserRequest) (resp *user_service.GetListUserResponse, err error) {

	i.log.Info("---GetUsers------>", logger.Any("req", req))

	resp, err = i.strg.User().GetAll(ctx, req)
	if err != nil {
		i.log.Error("!!!GetUsers->User->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *UserService) Update(ctx context.Context, req *user_service.UpdateUser) (resp *user_service.User, err error) {

	i.log.Info("---UpdateUser------>", logger.Any("req", req))

	rowsAffected, err := i.strg.User().Update(ctx, req)

	if err != nil {
		i.log.Error("!!!UpdateUser--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.User().GetByPKey(ctx, &user_service.UserPrimaryKey{UserId: req.UserId})
	if err != nil {
		i.log.Error("!!!GetUser->User->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *UserService) UpdatePatch(ctx context.Context, req *user_service.UpdatePatchUser) (resp *user_service.User, err error) {

	i.log.Info("---UpdatePatchEnrolledStudent------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.User().UpdatePatch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchUser--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.User().GetByPKey(ctx, &user_service.UserPrimaryKey{UserId: req.Id})
	if err != nil {
		i.log.Error("!!!GetUser->User->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (i *UserService) Delete(ctx context.Context, req *user_service.UserPrimaryKey) (resp *empty.Empty, err error) {

	i.log.Info("---DeleteUser------>", logger.Any("req", req))

	err = i.strg.User().Delete(ctx, req)
	if err != nil {
		i.log.Error("!!!DeleteUser->User->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &empty.Empty{}, nil
}

func (i *UserService) Login(ctx context.Context, req *user_service.LoginReq) (resp *user_service.User, err error) {

	i.log.Info("---Login------>", logger.Any("req", req))

	resp, err = i.strg.User().Login(ctx, req)

	if err.Error() == "no rows in result set" {
		i.log.Error("!!!Logi->User->Get--->", logger.Error(errors.New("such user not found")))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if err != nil {
		i.log.Error("!!!Logi->User->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}

func (i *UserService) Check(ctx context.Context, req *user_service.CheckExist) (resp *user_service.CheckExistRes, err error) {

	i.log.Info("---Check------>", logger.Any("req", req))

	resp, err = i.strg.User().ChechExist(ctx, req)
	if err != nil {
		i.log.Error("!!!Check->User->Get--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return
}
